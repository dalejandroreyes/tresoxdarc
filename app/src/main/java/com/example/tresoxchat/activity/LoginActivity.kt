package com.example.tresoxchat.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.tresoxchat.R
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()

        val buttonLogin: Button = findViewById(R.id.BtnSignIn)
        val edtEmailLog : EditText = findViewById(R.id.EdtEmailLog)
        val edtPassLog : EditText = findViewById(R.id.EdtPassLog)
        val buttonLogToRegister : TextView = findViewById(R.id.BtnSignUpLog)

        buttonLogin.setOnClickListener{
            val email = edtEmailLog.text.toString()
            val password = edtPassLog.text.toString()

            if(TextUtils.isEmpty(email) && TextUtils.isEmpty(password)){
                Toast.makeText(applicationContext,"Emaill and password are requiered",Toast.LENGTH_SHORT).show()
            }else{
                auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this){
                        if(it.isSuccessful){
                            edtEmailLog.setText("")
                            edtPassLog.setText("")
                            val intent = Intent(this@LoginActivity, ContentActivity::class.java)
                            startActivity(intent)
                        }else{
                            Toast.makeText(applicationContext,"Emaill or password invalid",Toast.LENGTH_SHORT).show()
                        }
                    }
            }
        }

        buttonLogToRegister.setOnClickListener{
            val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
            startActivity(intent)
        }

    }
}