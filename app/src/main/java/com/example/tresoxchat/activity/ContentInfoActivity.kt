package com.example.tresoxchat.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.tresoxchat.R

class ContentInfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content_info)
    }
}