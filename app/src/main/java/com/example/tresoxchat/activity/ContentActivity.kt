package com.example.tresoxchat.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import com.example.tresoxchat.R
import com.synnapps.carouselview.CarouselView

class ContentActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)

        val buttonContentInfo: CarouselView = findViewById(R.id.carouselView)

        val buttonChat: ImageButton = findViewById(R.id.BtnChat)

        buttonContentInfo.setOnClickListener {
            val intent = Intent(this@ContentActivity, ContentInfoActivity::class.java)
            startActivity(intent)
        }

        buttonChat.setOnClickListener{
            val intent = Intent(this@ContentActivity, ListUsersActivity::class.java)
            startActivity(intent)
        }



    }
}
