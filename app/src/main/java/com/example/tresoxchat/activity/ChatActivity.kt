package com.example.tresoxchat.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tresoxchat.R
import com.example.tresoxchat.adapter.ChatAdapter
import com.example.tresoxchat.model.Chat
import com.example.tresoxchat.model.user
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import de.hdodenhof.circleimageview.CircleImageView

class ChatActivity : AppCompatActivity() {
    var firebaseUser: FirebaseUser? = null
    var reference: DatabaseReference? = null
    var chatList = ArrayList<Chat>()
    var topic = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        val chatRecyclerView : RecyclerView = findViewById(R.id.chatRecyclerView)
        chatRecyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        var intent = getIntent()
        var userId = intent.getStringExtra("userId")
        var imgProfile: CircleImageView = findViewById(R.id.imgProfile)
        var tvUsername: TextView = findViewById(R.id.tvUserName)
        var imgBack: ImageView = findViewById(R.id.imgBack)
        var btnSendMessage : ImageButton = findViewById(R.id.btnSendMessage)
        var etMessage : EditText = findViewById(R.id.etMessage)

        firebaseUser = FirebaseAuth.getInstance().currentUser
        reference = FirebaseDatabase.getInstance().getReference("Users").child(userId!!)

        imgBack.setOnClickListener {
            onBackPressed()
        }

        reference!!.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onDataChange(snapshot: DataSnapshot) {

                val user = snapshot.getValue(user::class.java)
                tvUsername.text = user!!.userName
                if (user.profileImage == "") {
                    imgProfile.setImageResource(R.drawable.profile_image)
                } else {
                    Glide.with(this@ChatActivity).load(user.profileImage).into(imgProfile)

                }

            }
        })

            btnSendMessage.setOnClickListener {
                var message: String = etMessage.text.toString()
                if (message.isEmpty()){
                    Toast.makeText(applicationContext,"message is empty", Toast.LENGTH_SHORT).show()
                    etMessage.setText("")
                }else{
                    sendMessage(firebaseUser!!.uid,userId,message)
                    etMessage.setText("")
                }
            }

        readMessage(firebaseUser!!.uid,userId)
    }

            private fun sendMessage(senderId: String, receiverId: String, message: String) {
                var reference: DatabaseReference? = FirebaseDatabase.getInstance().getReference()

                var hashMap: HashMap<String, String> = HashMap()
                hashMap.put("senderId", senderId)
                hashMap.put("receiverId", receiverId)
                hashMap.put("message", message)

                reference!!.child("Chat").push().setValue(hashMap)

            }

    fun readMessage(senderId: String, receiverId: String) {
        val databaseReference: DatabaseReference =
            FirebaseDatabase.getInstance().getReference("Chat")

        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                chatList.clear()
                for (dataSnapShot: DataSnapshot in snapshot.children) {
                    val chat = dataSnapShot.getValue(Chat::class.java)

                    if (chat!!.senderId.equals(senderId) && chat!!.receiverId.equals(receiverId) ||
                        chat!!.senderId.equals(receiverId) && chat!!.receiverId.equals(senderId)
                    ) {
                        chatList.add(chat)
                    }
                }
                val chatRecyclerView : RecyclerView = findViewById(R.id.chatRecyclerView)
                val chatAdapter = ChatAdapter(this@ChatActivity, chatList)

                chatRecyclerView.adapter = chatAdapter
            }
        })
    }
}