package com.example.tresoxchat.activity

import android.annotation.SuppressLint
import android.icu.lang.UCharacter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tresoxchat.R
import com.example.tresoxchat.adapter.UserAdapter
import com.example.tresoxchat.model.user
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*

class ListUsersActivity : AppCompatActivity() {

    val userList = ArrayList<user>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_users)

        val userRecyclerView : RecyclerView = findViewById(R.id.userRecyclerView)
        var imgBack: ImageView = findViewById(R.id.imgBack)

        userRecyclerView.layoutManager = LinearLayoutManager(this,RecyclerView.VERTICAL, false)

        getUsersList()

        imgBack.setOnClickListener {
            onBackPressed()
        }

    }

    fun getUsersList(){
        val firebase: FirebaseUser = FirebaseAuth.getInstance().currentUser!!
        val databaseReference:DatabaseReference = FirebaseDatabase.getInstance().getReference("Users")

        databaseReference.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(applicationContext,error.message,Toast.LENGTH_SHORT).show()
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                userList.clear()
                val userRecyclerView2 : RecyclerView = findViewById(R.id.userRecyclerView)
                for(dataSnapshot:DataSnapshot in snapshot.children){
                    val user = dataSnapshot.getValue(user::class.java)
                        userList.add(user!!)
                }

                val userAdapter = UserAdapter(this@ListUsersActivity,userList)

                userRecyclerView2.adapter = userAdapter

            }

        })
    }
}